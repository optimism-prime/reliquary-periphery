// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8;

import "forge-std/Test.sol";
import "forge-std/console.sol";
import "src/VelodromeV2StakedERC20Factory.sol";
import "@openzeppelin/contracts/access/IAccessControl.sol";
import "@openzeppelin/contracts/mocks/token/ERC20Mock.sol";
import "@openzeppelin/contracts/utils/Strings.sol";
import {VelodromeV2Gauge} from "test/VelodromeV2StakedERC20.t.sol";

contract VelodromeV2StakedERC20FactoryTest is Test {
    VelodromeV2StakedERC20Factory public factory;

    address public constant deployer = address(0xc2ba);
    address public constant operator = address(0xab2c);

    function setUp() public {
        factory = new VelodromeV2StakedERC20Factory();
        factory.grantRole(factory.DEPLOYER(), deployer);
        factory.grantRole(factory.OPERATOR(), operator);
    }

    function test_onlyDeployerCanDeployToken(address caller, bool withAllArgs) public {
        vm.assume(caller != deployer);
        VelodromeV2Gauge gauge = new VelodromeV2Gauge(address(new ERC20Mock()), address(new ERC20Mock()));

        vm.expectRevert(
            abi.encodeWithSelector(IAccessControl.AccessControlUnauthorizedAccount.selector, caller, factory.DEPLOYER())
        );
        vm.prank(caller);
        if (withAllArgs) {
            factory.deployToken(gauge, "Name", "Symbol");
        } else {
            factory.deployToken(gauge);
        }
    }

    function test_onlyDeployerCanAddToken(address caller, address tokenAddr) public {
        vm.assume(caller != deployer);
        vm.expectRevert(
            abi.encodeWithSelector(IAccessControl.AccessControlUnauthorizedAccount.selector, caller, factory.DEPLOYER())
        );
        vm.prank(caller);
        factory.addToken(VelodromeV2StakedERC20(tokenAddr));
    }

    function test_onlyDeployerCanRemoveToken(address caller, uint256 index) public {
        vm.assume(caller != deployer);
        vm.expectRevert(
            abi.encodeWithSelector(IAccessControl.AccessControlUnauthorizedAccount.selector, caller, factory.DEPLOYER())
        );
        vm.prank(caller);
        factory.removeToken(index);
    }

    function test_deployTokenInstantiatesTokenContractAndAddToArray(uint160 gaugeCount, bool withAllArgs) public {
        vm.assume(gaugeCount < 16);

        for (uint160 i = 0; i < gaugeCount; ++i) {
            ERC20Mock rewardToken = new ERC20Mock();
            ERC20Mock stakingToken = new ERC20Mock();
            IVelodromeV2Gauge gauge = new VelodromeV2Gauge(address(rewardToken), address(stakingToken));
            string memory name;
            string memory symbol;
            if (withAllArgs) {
                name = string.concat("Staked ", Strings.toString(i));
                symbol = string.concat("stk-", Strings.toString(i));

                vm.prank(deployer);
                factory.deployToken(gauge, name, symbol);
            } else {
                name = string.concat("Staked ", stakingToken.name());
                symbol = string.concat("stk-", stakingToken.symbol());

                vm.prank(deployer);
                factory.deployToken(gauge);
            }
            assertEq(address(factory.tokens(i).gauge()), address(gauge));
            assertEq(factory.tokens(i).name(), name);
            assertEq(factory.tokens(i).symbol(), symbol);
        }
        assertEq(factory.tokenCount(), gaugeCount);
    }

    function test_addToken(uint160 gaugeCount) public {
        vm.assume(gaugeCount < 16);

        for (uint160 i = 0; i < gaugeCount; ++i) {
            vm.prank(deployer);
            factory.addToken(VelodromeV2StakedERC20(address(i)));
            assertEq(address(factory.tokens(i)), address(i));
        }
        assertEq(factory.tokenCount(), gaugeCount);
    }

    function test_removeToken(uint160 gaugeCount, uint256 index) public {
        vm.assume(gaugeCount < 16);
        vm.assume(gaugeCount > 0);
        vm.assume(index < gaugeCount);

        for (uint160 i = 0; i < gaugeCount; ++i) {
            vm.prank(deployer);
            factory.addToken(VelodromeV2StakedERC20(address(i)));
        }

        vm.prank(deployer);
        factory.removeToken(index);

        for (uint160 i = 0; i < gaugeCount - 1; ++i) {
            if (i < index) {
                assertEq(address(factory.tokens(i)), address(i));
            } else {
                assertEq(address(factory.tokens(i)), address(i + 1));
            }
        }
        assertEq(factory.tokenCount(), gaugeCount - 1);
    }

    function test_onlyOperatorCanGetRewards(address caller, address recipient, bool withAllArgs) public {
        vm.assume(caller != operator);
        vm.expectRevert(
            abi.encodeWithSelector(IAccessControl.AccessControlUnauthorizedAccount.selector, caller, factory.OPERATOR())
        );
        vm.prank(caller);
        if (withAllArgs) {
            VelodromeV2StakedERC20[] memory tokens;
            factory.getReward(tokens, recipient);
        } else {
            factory.getReward(recipient);
        }
    }

    function test_GetRewardsClaimGauges(
        uint8 gaugeCount,
        address recipient,
        uint8 index0,
        uint128 count0,
        uint8 index1,
        uint128 count1,
        address otherRecipient
    ) public {
        vm.assume(gaugeCount < 16);
        vm.assume(index0 < gaugeCount);
        vm.assume(index1 < gaugeCount);
        vm.assume(index0 != index1);
        vm.assume(recipient != address(0));
        vm.assume(otherRecipient != address(0));
        vm.assume(otherRecipient != recipient);
        for (uint8 i = 0; i < gaugeCount; ++i) {
            ERC20Mock rewardToken = new ERC20Mock();
            ERC20Mock stakingToken = new ERC20Mock();
            IVelodromeV2Gauge gauge = new VelodromeV2Gauge(address(rewardToken), address(stakingToken));
            rewardToken.mint(address(gauge), i + 1);

            string memory name = string.concat("Staked ", Strings.toString(i));
            string memory symbol = string.concat("stk-", Strings.toString(i));

            vm.prank(deployer);
            factory.deployToken(gauge, name, symbol);
        }

        vm.prank(operator);
        factory.getReward(recipient);

        for (uint8 i = 0; i < gaugeCount; ++i) {
            IVelodromeV2Gauge gauge = factory.tokens(i).gauge();
            IERC20 rewardToken = IERC20(gauge.rewardToken());
            assertEq(rewardToken.balanceOf(address(gauge)), 0);
            assertEq(rewardToken.balanceOf(recipient), i + 1);
        }

        VelodromeV2StakedERC20[] memory tokensToClaim = new VelodromeV2StakedERC20[](2);
        tokensToClaim[0] = factory.tokens(index0);
        tokensToClaim[1] = factory.tokens(index1);

        ERC20Mock rewardToken0 = ERC20Mock(tokensToClaim[0].gauge().rewardToken());
        ERC20Mock rewardToken1 = ERC20Mock(tokensToClaim[1].gauge().rewardToken());

        rewardToken0.mint(address(tokensToClaim[0].gauge()), count0);
        rewardToken1.mint(address(tokensToClaim[1].gauge()), count1);

        vm.prank(operator);
        factory.getReward(tokensToClaim, otherRecipient);

        assertEq(rewardToken0.balanceOf(address(tokensToClaim[0].gauge())), 0);
        assertEq(rewardToken0.balanceOf(address(otherRecipient)), count0);

        assertEq(rewardToken1.balanceOf(address(tokensToClaim[1].gauge())), 0);
        assertEq(rewardToken1.balanceOf(address(otherRecipient)), count1);
    }
}
