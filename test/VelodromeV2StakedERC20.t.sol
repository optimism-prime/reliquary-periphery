// SPDX-License-Identifier: Unlicense
pragma solidity ^0.8;

import "forge-std/Test.sol";
import "forge-std/console.sol";
import "src/VelodromeV2StakedERC20.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/mocks/token/ERC20Mock.sol";
import "@openzeppelin/contracts/mocks/token/ERC20DecimalsMock.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract VelodromeV2StakedERC20Test is Test {
    VelodromeV2StakedERC20 public token;
    VelodromeV2Gauge public gauge;

    ERC20Mock rewardToken;
    ERC20Mock stakingToken;

    function setUp() public {
        rewardToken = new ERC20Mock();
        stakingToken = new ERC20Mock();
        gauge = new VelodromeV2Gauge(address(rewardToken), address(stakingToken));
        token = new VelodromeV2StakedERC20();
        token.initialize(gauge, "StakedToken", "STK-TKN");
    }

    function test_WhenAlreadyInitialized_InitializeFail() public {
        vm.expectRevert(Initializable.InvalidInitialization.selector);
        token.initialize(gauge, "StakedToken2", "STK-TKN2");
    }

    function test_StakedTokenDecimalsMatchesTokenDecimals(uint8 decimals) public {
        ERC20DecimalsMock otherStakingToken = new MyERC20DecimalsMock(decimals);
        VelodromeV2Gauge otherGauge = new VelodromeV2Gauge(address(rewardToken), address(otherStakingToken));
        VelodromeV2StakedERC20 otherToken = new VelodromeV2StakedERC20();
        otherToken.initialize(otherGauge, "StakedToken", "STK-TKN");

        assertEq(otherToken.decimals(), decimals);
    }

    struct DepositConfig {
        uint256 balance;
        uint256 amount;
        address account;
        address otherAccount;
    }

    function setupDeposit(DepositConfig memory config) public {
        // Initial mint of staking token for account
        stakingToken.mint(config.account, config.balance);

        vm.startPrank(config.account);
        // Mint staked token using account balance, but for otherAccount
        stakingToken.approve(address(token), config.amount);
        token.depositFor(config.otherAccount, config.amount);
        vm.stopPrank();
    }

    function test_WhenDeposit_TokensAreTransferedInAndStakedInGauge(uint256 balance, uint256 amount) public {
        vm.assume(amount < balance);
        vm.assume(amount > 0);

        DepositConfig memory config =
            DepositConfig({balance: balance, amount: amount, account: address(0xc2ba), otherAccount: address(0xab2c)});
        setupDeposit(config);

        // Ensure staking tokens have been transfered to gauge
        assertEq(stakingToken.balanceOf(config.account), config.balance - config.amount);
        assertEq(stakingToken.balanceOf(address(token)), 0);
        assertEq(stakingToken.balanceOf(address(gauge)), config.amount);
        assertEq(gauge.balanceOf(address(token)), config.amount);

        // Ensure staked token balances are correct
        assertEq(token.balanceOf(config.account), 0);
        assertEq(token.balanceOf(config.otherAccount), config.amount);
        assertEq(token.totalSupply(), config.amount);
    }

    function test_WhenInsufficientTokenBalance_ThenDepositFails(uint256 amount) public {
        vm.assume(amount > 0);
        stakingToken.approve(address(token), amount);

        vm.expectRevert(
            abi.encodeWithSelector(IERC20Errors.ERC20InsufficientBalance.selector, address(this), 0, amount)
        );
        token.depositFor(address(this), amount);
    }

    function test_WhenWithdraw_TokensAreUnstakedFromGaugeAndTransferedOut(
        uint256 balance,
        uint256 amount,
        uint256 withdrawAmount
    ) public {
        vm.assume(amount < balance);
        vm.assume(amount > 0);
        vm.assume(withdrawAmount <= amount);

        DepositConfig memory config =
            DepositConfig({balance: balance, amount: amount, account: address(0xc2ba), otherAccount: address(0xab2c)});
        setupDeposit(config);

        vm.prank(config.otherAccount);
        token.withdrawTo(address(this), withdrawAmount);
        assertEq(stakingToken.balanceOf(address(this)), withdrawAmount);

        // Ensure staking tokens have been transfered out of gauge
        assertEq(stakingToken.balanceOf(address(gauge)), config.amount - withdrawAmount);
        assertEq(gauge.balanceOf(address(token)), config.amount - withdrawAmount);

        // Ensure staked token balances are correct
        assertEq(token.balanceOf(config.otherAccount), config.amount - withdrawAmount);
        assertEq(token.totalSupply(), config.amount - withdrawAmount);
    }

    function test_WhenInsufficientStakedTokenBalance_ThenWithdrawFails(
        uint128 balance,
        uint128 amount,
        uint128 deltaAmount
    ) public {
        vm.assume(amount < balance);
        vm.assume(amount > 0);
        vm.assume(deltaAmount > 0);

        DepositConfig memory config =
            DepositConfig({balance: balance, amount: amount, account: address(0xc2ba), otherAccount: address(0xab2c)});
        setupDeposit(config);

        uint256 withdrawAmount = uint256(amount) + deltaAmount;

        // Ensure the gauge has enough tokens, so the failure does not come from
        // withdrawing too many tokens from the gauge, but from config.otherAccount not
        // having enough tokens.
        stakingToken.mint(address(this), deltaAmount);
        stakingToken.approve(address(token), deltaAmount);
        token.depositFor(address(this), deltaAmount);

        vm.prank(config.otherAccount);
        vm.expectRevert(
            abi.encodeWithSelector(
                IERC20Errors.ERC20InsufficientBalance.selector, address(config.otherAccount), amount, withdrawAmount
            )
        );
        token.withdrawTo(address(this), withdrawAmount);
    }

    function test_GetRewardIsRestrictedToOwner(address caller, address recipient) public {
        vm.assume(caller != address(this));
        vm.expectRevert(abi.encodeWithSelector(Ownable.OwnableUnauthorizedAccount.selector, caller));
        vm.prank(caller);
        token.getReward(recipient);
    }

    function test_GetRewardTransfersRewardsFromGaugeToRecipient(uint256 rewardTokenAmount, address recipient) public {
        vm.assume(recipient != address(0));
        vm.assume(recipient != address(gauge));
        rewardToken.mint(address(gauge), rewardTokenAmount);
        token.getReward(recipient);
        assertEq(rewardToken.balanceOf(recipient), rewardTokenAmount);
        assertEq(rewardToken.balanceOf(address(gauge)), 0);
    }
}

contract VelodromeV2Gauge is IVelodromeV2Gauge {
    address public rewardToken;
    address public stakingToken;

    mapping(address => uint256) public balanceOf;

    constructor(address rewardToken_, address stakingToken_) {
        rewardToken = rewardToken_;
        stakingToken = stakingToken_;
    }

    function getReward(address account) external {
        IERC20(rewardToken).transfer(account, IERC20(rewardToken).balanceOf(address(this)));
    }

    function deposit(uint256 amount) external {
        require(amount > 0);
        balanceOf[msg.sender] += amount;
        IERC20(stakingToken).transferFrom(msg.sender, address(this), amount);
    }

    function withdraw(uint256 amount) external {
        balanceOf[msg.sender] -= amount;
        IERC20(stakingToken).transfer(msg.sender, amount);
    }
}

contract MyERC20DecimalsMock is ERC20DecimalsMock {
    constructor(uint8 _decimals) ERC20("ERC20Mock", "E20M") ERC20DecimalsMock(_decimals) {}
}
