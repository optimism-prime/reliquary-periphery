// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8;

import "@openzeppelin/contracts/access/extensions/AccessControlEnumerable.sol";
import {Clones} from "@openzeppelin/contracts/proxy/Clones.sol";
import "src/VelodromeV2StakedERC20.sol";

contract VelodromeV2StakedERC20Factory is AccessControlEnumerable {
    using Clones for address;

    bytes32 public constant DEPLOYER = keccak256("DEPLOYER");
    bytes32 public constant OPERATOR = keccak256("OPERATOR");

    address public instance;
    VelodromeV2StakedERC20[] public tokens;

    event TokenDeployed(uint256 indexed idx, address indexed gauge, address indexed addr, string name, string symbol);

    constructor() {
        instance = address(new VelodromeV2StakedERC20());
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
    }

    function deployToken(IVelodromeV2Gauge _gauge) external onlyRole(DEPLOYER) returns (VelodromeV2StakedERC20 token) {
        IERC20Metadata metadata = IERC20Metadata(_gauge.stakingToken());
        string memory _name = string.concat("Staked ", metadata.name());
        string memory _symbol = string.concat("stk-", metadata.symbol());
        return _deployToken(_gauge, _name, _symbol);
    }

    function deployToken(IVelodromeV2Gauge _gauge, string memory _name, string memory _symbol)
        external
        onlyRole(DEPLOYER)
        returns (VelodromeV2StakedERC20 token)
    {
        return _deployToken(_gauge, _name, _symbol);
    }

    function _deployToken(IVelodromeV2Gauge _gauge, string memory _name, string memory _symbol)
        internal
        returns (VelodromeV2StakedERC20 token)
    {
        token = VelodromeV2StakedERC20(instance.clone());
        token.initialize(_gauge, _name, _symbol);
        tokens.push(token);
        emit TokenDeployed(tokens.length - 1, address(_gauge), address(token), _name, _symbol);
    }

    function addToken(VelodromeV2StakedERC20 token) external onlyRole(DEPLOYER) {
        tokens.push(token);
    }

    function removeToken(uint256 index) external onlyRole(DEPLOYER) {
        if (index >= tokens.length) return;
        for (uint256 i = index; i < tokens.length - 1; ++i) {
            tokens[i] = tokens[i + 1];
        }
        tokens.pop();
    }

    function tokenCount() external view returns (uint256) {
        return tokens.length;
    }

    function getReward(VelodromeV2StakedERC20[] memory _tokens, address _recipient) external onlyRole(OPERATOR) {
        _getReward(_tokens, _recipient);
    }

    function getReward(address _recipient) external onlyRole(OPERATOR) {
        _getReward(tokens, _recipient);
    }

    function _getReward(VelodromeV2StakedERC20[] memory _tokens, address _recipient) internal {
        for (uint256 i = 0; i < _tokens.length; ++i) {
            _tokens[i].getReward(_recipient);
        }
    }
}
