// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8;

import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Permit.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/ERC20Burnable.sol";
import "@openzeppelin/contracts/access/extensions/AccessControlEnumerable.sol";

contract Energon is ERC20Permit, AccessControlEnumerable {
    bytes32 public constant MINTER = keccak256("MINTER");

    constructor() ERC20("Energon", "ENG") ERC20Permit("Energon") {
        _grantRole(DEFAULT_ADMIN_ROLE, msg.sender);
    }

    function mint(address account, uint256 amount) external onlyRole(MINTER) {
        _mint(account, amount);
    }

    function burnFrom(address account, uint256 value) external onlyRole(MINTER) {
        _spendAllowance(account, msg.sender, value);
        _burn(account, value);
    }
}
