// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8;

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";

contract EnergonMinterFromOPETH {
    using SafeERC20 for IERC20;

    IERC20 public constant OP;
    IERC20 public constant WETH;
    // OPETH

    // Factory

    function mint(uint256 amountOPDesired, uint256 amountWETHDesired) external {}

    function redeem() external {}
}
