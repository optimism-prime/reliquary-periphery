// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/IERC20Metadata.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/ERC20PermitUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/extensions/ERC20WrapperUpgradeable.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/IERC20Permit.sol";

contract VelodromeV2StakedERC20 is ERC20PermitUpgradeable, ERC20WrapperUpgradeable, OwnableUpgradeable {
    using SafeERC20 for IERC20;

    IVelodromeV2Gauge public gauge;

    function initialize(IVelodromeV2Gauge _gauge, string memory _name, string memory _symbol) external initializer {
        __ERC20_init(_name, _symbol);
        __ERC20Permit_init(_name);
        __ERC20Wrapper_init(IERC20(_gauge.stakingToken()));
        __Ownable_init(msg.sender);
        gauge = _gauge;
    }

    function decimals() public view override(ERC20Upgradeable, ERC20WrapperUpgradeable) returns (uint8) {
        return super.decimals();
    }

    function depositFor(address account, uint256 value) public override returns (bool) {
        super.depositFor(account, value);
        underlying().approve(address(gauge), value);
        gauge.deposit(value);
        return true;
    }

    function withdrawTo(address account, uint256 value) public override returns (bool) {
        gauge.withdraw(value);
        super.withdrawTo(account, value);
        return true;
    }

    function getReward(address _recipient) external onlyOwner {
        IVelodromeV2Gauge(gauge).getReward(address(this));
        IERC20 rewardToken = IERC20(IVelodromeV2Gauge(gauge).rewardToken());
        rewardToken.safeTransfer(_recipient, rewardToken.balanceOf(address(this)));
    }
}

interface IVelodromeV2Gauge {
    function getReward(address account) external;

    function deposit(uint256 amount) external;

    function withdraw(uint256 amount) external;

    function stakingToken() external view returns (address); // Returns the LP token to stake

    function rewardToken() external view returns (address);
}
